#include "mainwindow.h"
#include <QMenuBar>
#include<QTimerEvent>
#include <QMessageBox>
#include<QPushButton>
#include<QDebug>
#include "handle.h"

#define START_X        100
#define START_Y        100
#define BLOCKSIZE      100//每个方格都是边长为100的正方形

MainWindow::MainWindow(QWidget *parent) //设置菜单
    : QMainWindow(parent)
{
QMenuBar *bar=menuBar();//创建菜单栏
mainMenu_= bar->addMenu("重新开始"); //  “mainMenu_”在头文件"mainwindow.h"中定义了

 setMinimumSize(400,400);//设置窗口大小
 setWindowTitle("数字华容道");//设置标题

    connect(mainMenu_,&QMenu::aboutToShow,this,&MainWindow::OnMainMenuClickedActionSlot);

   rowNum_ = 3;//默认初始值
   colNum_ = 3;
   NewGame();
}

MainWindow::~MainWindow(){}//析构函数


void MainWindow::OnMainMenuClickedActionSlot(){//重新开始游戏
    NewGame();
}


//重新开始游戏（随机数生成）函数
void MainWindow::NewGame(){
   resize(START_X*2 + colNum_*BLOCKSIZE,START_Y*2 + rowNum_*BLOCKSIZE);  //"resize"重新定义窗口大小
    ReleaseItems();
    InitItems();
}

//画背景
void MainWindow::paintEvent(QPaintEvent *){
    QPainter painter(this);
    paintGrid();
    paintItems();

    update();
}



void MainWindow::ReleaseItems()//释放数组。需要手动释放内存，因为对象树是在回收了窗口的情况下，才会自动释放内存。而我们这里还没玩完呢，不能对窗口进行回收。
{
    for(int i = 0; i < items_.size(); i++){
        for(int j = 0; j < items_[i].size(); j++) {
            if(items_[i][j] == nullptr){
                delete items_[i][j];
                items_[i][j] = nullptr;
            }
        }
    }

    items_.clear();
}

void MainWindow::InitItems()
{
    numVec_.clear();//"clean"函数，仅将内容清空，长度不变
    while (true){
        int num = rand()%(colNum_*rowNum_);//生成0~colNum_*rowNum_个随机数，其中0为空白格
        if(!numVec_.contains(num)){ //如果numVec_没有num这个数
            numVec_.push_back(num);//就把这个数放进数组里
        }
        if(numVec_.size() == colNum_*rowNum_){
                break;

        }
    }


    for(int i = 0; i < colNum_;i++) {
        QVector<Item*> itemVec;//itemVec充当赋值的中间值
        for(int j = 0; j < rowNum_; ++j){
            Item* item = new Item(QPoint(i,j),numVec_[i*colNum_ + j]);//给动态生成出来的Item型数组填充坐标和数值
            itemVec.push_back(item);//把item这个指针不断赋给itemVec数组
            if(numVec_[i*colNum_ + j] == 0)//是空格
            {
                emptyPoint_ = QPoint(i,j);
            }
        }
        items_.push_back(itemVec);//items_是指针数组
    }
}



void MainWindow::paintGrid(){//绘制格子
    QPainter painter(this);
    painter.setPen(QPen(Qt::black,2));//划线
    painter.setBrush(Qt::white);//填色

    for(int i = 0; i < colNum_; ++i)
    {
        for(int j = 0; j < rowNum_; ++j)
        {
            painter.drawRect(START_X+i*BLOCKSIZE,START_Y+j*BLOCKSIZE,BLOCKSIZE,BLOCKSIZE);//“drawRect”绘制矩形

        }
    }
}

void MainWindow::paintItems(){//绘制数字
    QPainter painter(this);
    QFont font;//调整字体
    font.setPointSize(30);//字的大小
    font.setFamily(("Droid Serif"));//字体
    font.setBold(true);//字体加粗

    for(int i = 0; i < colNum_; ++i){
        for(int j = 0; j < rowNum_; ++j){

            Item* currItem = items_[i][j];
            if(currItem->num_ == 0)
            {
                //do nothing
            }
            else
            {
                int num = currItem->num_;
                QString str = QString::number(num);//数字转换为字符串
                painter.setFont(font);
                painter.drawText(START_X + currItem->pos_.x()*BLOCKSIZE,START_Y + currItem->pos_.y()*BLOCKSIZE+20,BLOCKSIZE,BLOCKSIZE,Qt::AlignHCenter,str);//绘制文本
            }
        }
    }
}





