#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include "item.h"
#include <QPainter>
#include <QMouseEvent>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void OnMainMenuClickedActionSlot();
protected:
    void paintEvent(QPaintEvent *);
private:
    void NewGame();//重新开始
    void ReleaseItems();
    void InitItems();

    void paintGrid();
    void paintItems();
private:
    QMenu* mainMenu_;//重新开始
private:
    int rowNum_;//行数
    int colNum_;//列数

    QVector<QVector<Item*>> items_;//容器类：QVector<T>，和普通数组差不多，但是它的大小可以重新定义。
    QVector<int> numVec_;
    QPoint emptyPoint_;//空方格位置
};

#endif // MAINWINDOW_H
